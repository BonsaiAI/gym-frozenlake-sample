import time
import gym
from functools import reduce

import bonsai
from bonsai_gym_common import GymSimulator
from gym.envs.registration import register

ENVIRONMENT = 'FrozenLake-no-slip-v0'
RECORD_PATH = None
RECORDING_TIME = 40*60*60

# Map types: 4x4 or 8x8.
MAP_4x4 = '4x4'
MAP_8x8 = '8x8'

# Whether or not choose the non-deterministic states.
IS_SLIPPERY = False

# Scale coefficients for reward.
REWARD_SCALE = 10


# Register a custom environment for frozen lake with deterministic states
register(
    id=ENVIRONMENT,
    entry_point='gym.envs.toy_text:FrozenLakeEnv',
    kwargs={'map_name': MAP_4x4,
            'is_slippery': IS_SLIPPERY},
    timestep_limit=100,
    reward_threshold=0.78,
)


class FrozenLakeSimulator(GymSimulator):

    def __init__(self, env, record_path, render_env):
        GymSimulator.__init__(
            self, env, skip_frame=1,
            record_path=record_path, render_env=render_env)

    def advance(self, actions):
        # Step 1: Perform the action and update the game along with
        # the reward.
        average_reward = 0
        for i in range(self._skip_frame):
            if self._terminal:
                self._action = 0
            observation, reward, done, info = self.env.step(
                self.get_gym_action(actions))
            self._frame_count += 1
            average_reward += REWARD_SCALE * reward
            self.gym_total_reward += reward

            # Step 2: Render the game.
            if self._render_env:
                self.env.render()

            if done:
                break
        self._reward = average_reward / (i + 1)

        time_from_start = time.time() - self._start_time
        if self._is_recording and (time_from_start > RECORDING_TIME):
            self.env.monitor.close()

        # Step 3: Get the current frames, and append it to deque to get current
        # state.
        current_frame = self.process_observation(observation)
        self._append_state(current_frame)

        # Step 4: Check if we shousld reset
        self._check_terminal(done)

    def get_state(self):
        # We append all of the states in the deque by adding the rows.
        current_state = reduce(
            lambda accum, state: accum + state, self._state_deque)

        # Convert the observation to an inkling schema.
        state_dict = {"current_pos": self.get_state_schema(current_state)}
        return bonsai.simulator.SimState(state_dict, self._terminal)


if __name__ == "__main__":
    env = gym.make(ENVIRONMENT)
    base_args = bonsai.parse_base_arguments()
    simulator = FrozenLakeSimulator(env, RECORD_PATH, not base_args.headless)
    bonsai.run_for_training_or_prediction("frozenlake_simulator", simulator)
